@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-3">
            @include('users.filter')
        </div>
        <div class="col-md-9">
            @include('users.add-edit-user')
            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
            <div class="d-flex justify-content-end mb-3">
                <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#AddUser">
                    Add User
                </button>
            </div>
        </div>
    </div>

            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Address</th>
                        <th>Birthday</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>

                    @foreach($users as $user)
                        <tr id="user-{{$user->id}}">
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->email }}</td>
                            <td>{{ $user->address }}</td>
                            <td>{{ $user->birthday }}</td>
                            <td>
                                @include('users.action',['user' => $user])
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

</div>

<script>
    @if ($errors->any())
        $(document).ready(function() {
            $('#AddUser').modal('show');
        });
    @endif
</script>
@endsection
