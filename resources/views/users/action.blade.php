
    @include('users.add-edit-user')
    <button type="button"  data-bs-toggle="modal" data-bs-target="#UpdateUser-{{$user->id}}" class="mb-1 btn btn-success">update</button>
    <a href="javascript:void(0)" onclick="deletePost({{$user->id}})" class="mb-1 btn btn-danger">Delete</a>


    <script>
        function deletePost(id)
        {
            if(confirm("Do you really want to delete this record?"))
            {
                $.ajax({
                    type: "DELETE",
                    url: "/delete-user/" + id,
                    data: {
                        _token : $("input[name = _token]").val()
                    },
                    success: function (response) {
                        if (response.success) {
                            // Show success notification using Laravel's session
                            // You can replace 'success' with 'danger', 'warning', etc. depending on your needs
                            toastr.success('User Deleted Successfully');

                        $("#user-"+id).remove();
                        }
                    }
                });
            }
        }
    </script>

