<?php
namespace App\Repositories\Interface;

use App\Models\User;

interface UserRepositoryInterface
{
    public function getAllUsersSortedByBirthday();
    public function getUsersWithFilter(string $filter);

    public function createUser(array $data);

    public function updateUser(User $user, array $data);

    public function deleteUser($id);
    public function getByUpcomingBirthdays();
    public function getByUpcomingBirthdaysWithin7Days();
    public function getByFinishedBirthdaysLast7Days();
}
