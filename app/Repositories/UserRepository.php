<?php

namespace App\Repositories;

use App\Models\User;
use App\Repositories\Interface\UserRepositoryInterface;
use Carbon\Carbon;

class UserRepository implements UserRepositoryInterface
{
    public function getAllUsersSortedByBirthday()
    {
        return User::orderBy('birthday')->get();
    }

    public function createUser(array $data)
    {
        return User::create($data);
    }

    public function updateUser($userId, array $data)
    {
        $user = User::findOrFail($userId);
        $user->update($data);
    }

    public function deleteUser($id)
    {
        User::destroy($id);
    }
    public function getUsersWithFilter(string $filter)
    {
        switch ($filter) {
            case 'upcoming':
                return $this->getByUpcomingBirthdays();
            case 'upcoming_within_7_days':
                return $this->getByUpcomingBirthdaysWithin7Days();
            case 'finished_last_7_days':
                return $this->getByFinishedBirthdaysLast7Days();
            default:
                return $this->getAllUsersSortedByBirthday();
        }
    }

    public function getByUpcomingBirthdays()
    {
        $today = Carbon::today();
        return User::where('birthday', '>=', $today)
                    ->orderBy('birthday', 'asc')
                    ->get();
    }

    public function getByUpcomingBirthdaysWithin7Days()
    {
        $today = Carbon::today();
        $endOfWeek = Carbon::today()->addDays(7);
        return User::whereBetween('birthday', [$today, $endOfWeek])
                    ->orderBy('birthday', 'asc')
                    ->get();
    }

    public function getByFinishedBirthdaysLast7Days()
    {
        $today = Carbon::today();
        $startOfWeek = Carbon::today()->subDays(7);
        return User::whereBetween('birthday', [$startOfWeek, $today])
                    ->orderBy('birthday', 'asc')
                    ->get();
                    // ->toArray();
    }

}
