<form action="{{ route('user.index') }}" method="GET" class="mb-3">
    <div class="form-group">
        <label for="filter">Filter:</label>
        <select name="filter" id="filter" class="form-control">
            <option value="">All</option>
            <option value="upcoming">Upcoming Birthday</option>
            <option value="upcoming_within_7_days">Upcoming Birthday Within 7 Days</option>
            <option value="finished_last_7_days">Finished Birthday Last 7 Days</option>
        </select>
    </div>
    <button type="submit" class="btn btn-primary">Apply Filters</button>
</form>
