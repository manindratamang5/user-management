<!-- Modal -->
<div class="modal fade" id="{{ isset($user->id) ? 'UpdateUser-'.$user->id : 'AddUser' }}" tabindex="-1" aria-labelledby="AddUserModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg show">
    <div class="modal-content">
      <div class="modal-header">
        <h1 class="modal-title fs-5" id="AddUserLabel">{{ isset($user->id) ? 'Update User' : 'Add User' }}</h1>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">

        <form method="POST" action="{{ isset($user->id) ? route('user.update', $user->id) : route('user.store') }}">
            @csrf
            @if(isset($user->id))
            @method('PUT')
            @endif
            <div class="mb-3">
                <label for="name">Name</label>
                <input class="form-control" value="{{ old('name',$user->name ?? '') }}" type="text" id="name" name="name">
                @error('name')
                <div class="alert alert-danger mt-1">{{ $message }}</div>
                @enderror
            </div>
            <div class="mb-3">
                <label for="email">Email</label>
                <input class="form-control" value="{{ old('email',$user->email ?? '') }}" type="email" id="email" name="email">
                @error('email')
            <div class="alert alert-danger mt-1">{{ $message }}</div>
            @enderror
            </div>
            <div class="mb-3">
                <label for="address">Address</label>
                <input  class="form-control" type="text" value="{{ old('address',$user->address ?? '') }}" id="address" name="address">
                @error('address')
            <div class="alert alert-danger mt-1">{{ $message }}</div>
            @enderror
            </div>
            <div class="mb-3">
                <label for="birthday">Birthday</label>
                <input class="form-control" type="date" value="{{ old('birthday',$user->birthday ?? '') }}" id="birthday" name="birthday">
                @error('birthday')
            <div class="alert alert-danger mt-1">{{ $message }}</div>
            @enderror
            </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">{{ isset($user->id) ? 'Update' : 'Add' }}</button>
      </div>
    </form>
    </div>
  </div>
</div>

