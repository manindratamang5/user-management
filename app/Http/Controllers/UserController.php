<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Repositories\Interface\UserRepositoryInterface;
use Illuminate\Http\Request;

class UserController extends Controller
{
    private $userRepository;
    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }
    public function index(Request $request){
        $filter = $request->input('filter');

        $users = [];
        if (!empty($filter)) {
            $users = $this->userRepository->getUsersWithFilter($filter);
        } else {
            $users = $this->userRepository->getAllUsersSortedByBirthday();
        }
        return view('users.index', compact('users'));
    }
    public function store(UserRequest $request)
    {
        $this->userRepository->createUser($request->validated());

        return redirect()->route('user.index')->with('success', 'User Created Successfully.');

    }
    public function update(UserRequest $request, $id)
    {
        $data = $request->validated();

        $this->userRepository->updateUser($id, $data);

        return redirect()->route('user.index')->with('success', 'User Updated Successfully.');
    }
    public function destroy(Request $request,$id)
    {
        $this->userRepository->deleteUser($id);

        return response()->json(['success' => true]);
    }

}
